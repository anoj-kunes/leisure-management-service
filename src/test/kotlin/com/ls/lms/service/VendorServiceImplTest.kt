package com.ls.lms.service

import com.ls.lms.exception.DataNotFoundException
import com.ls.lms.mongo.entity.Vendor
import com.ls.lms.mongo.repository.VendorRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
@SpringBootTest(properties = arrayOf("spring.profiles.active=unit-test"))
class VendorServiceImplTest {

    @Mock
    lateinit var vendorRepository: VendorRepository

    @InjectMocks
    lateinit var vendorService: VendorServiceImpl

    @Test
    fun findById_idExists_methodCallsAndReturns() {
        val vendor = Vendor("5e246ab08362930f9a15ec2a", "lon-vendor", "London")
        Mockito.lenient().`when`(vendorRepository.findById("5e246ab08362930f9a15ec2a"))
                .thenReturn(Optional.of(vendor))

        val vendorDto = this.vendorService.findById("5e246ab08362930f9a15ec2a")
        Assertions.assertEquals(vendorDto.city, vendor.city)
        Mockito.verify(vendorRepository, Mockito.times(1)).findById("5e246ab08362930f9a15ec2a")
    }

    @Test
    fun findById_idNonExists_throwsDataNotFoundException() {
        Mockito.lenient().`when`(vendorRepository.findById("5e246ab08362930f9a15ec2a"))
                .thenReturn(Optional.empty())
        Assertions.assertThrows(DataNotFoundException::class.java, {
            this.vendorService.findById("5e246ab08362930f9a15ec2a")
        }, "Vendor not found for #5e246ab08362930f9a15ec2a")

        Mockito.verify(vendorRepository, Mockito.times(1)).findById("5e246ab08362930f9a15ec2a")
    }
}