package com.ls.lms.controller

import com.ls.lms.dto.CustomerDto
import com.ls.lms.exception.AppExceptionHandler
import com.ls.lms.exception.DataNotFoundException
import com.ls.lms.service.CustomerServiceImpl
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import java.util.*

@SpringBootTest(properties = arrayOf("spring.profiles.active=unit-test"))
@ExtendWith(MockitoExtension::class)
@EnableWebMvc
@ActiveProfiles("unit-test")
class CustomerControllerTest {
    lateinit var mockMvc: MockMvc
    @MockBean
    lateinit var customerService: CustomerServiceImpl

    @Autowired
    lateinit var customerController: CustomerController

    @Autowired
    lateinit var appExceptionHandler: AppExceptionHandler

    private val uri = "/api/v1/customers"

    @BeforeEach
    fun setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(customerController)
                .setMessageConverters(MappingJackson2HttpMessageConverter())
                .setControllerAdvice(appExceptionHandler)
                .addPlaceholderValue("app.api.url", "/api/v1").build()
    }

    @Test
    fun findById_idExists_methodCallsAndReturnsCustomerDto() {
        Mockito.lenient().doAnswer {
            CustomerDto("5e246ab08362930f9a15ec2a", "anojkunes@googlemail.com", "Anoj", "Kunes", "Birmingham")
        }.`when`(customerService).findById("5e246ab08362930f9a15ec2a")

        mockMvc.perform(MockMvcRequestBuilders.get("$uri/5e246ab08362930f9a15ec2a")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", Matchers.`is`("anojkunes@googlemail.com")))
    }

    @Test
    fun findById_idNonExists_throwsDataNotFoundException() {
        Mockito.lenient().doThrow(DataNotFoundException("Customer not found for #5e246ab08362930f9a15ec24a"))
        .`when`(customerService).findById("5e246ab08362930f9a15ec24a")

        mockMvc.perform(MockMvcRequestBuilders.get("$uri/5e246ab08362930f9a15ec24a")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound)
                .andExpect(MockMvcResultMatchers.jsonPath("$.type").value("DATA_NOT_AVAILABLE"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].error").value("Customer not found for #5e246ab08362930f9a15ec24a"))


    }
}