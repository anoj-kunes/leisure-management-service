package com.ls.lms.controller

import com.ls.lms.dto.CustomerDto
import com.ls.lms.dto.VendorDto
import com.ls.lms.exception.DataNotFoundException
import com.ls.lms.service.CustomerServiceImpl
import com.ls.lms.service.LeisureManagementService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("\${app.api.url}/vendors")
class VendorController {

    @Autowired
    @Qualifier("vendorServiceImpl")
    private lateinit var vendorService: LeisureManagementService<VendorDto>

    // get single vendor endpoint
    @Throws(DataNotFoundException::class)
    @GetMapping("/{id}")
    fun getCustomer(@PathVariable("id") id: String): VendorDto {
        return vendorService.findById(id)
    }
}