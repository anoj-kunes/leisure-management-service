package com.ls.lms.mongo.changelog

import com.github.mongobee.changeset.ChangeLog
import com.github.mongobee.changeset.ChangeSet
import com.ls.lms.mongo.entity.Customer
import com.ls.lms.mongo.entity.Vendor
import org.springframework.data.mongodb.core.MongoTemplate

@ChangeLog
class VendorMigrationChangeLog {
    @ChangeSet(order = "01", id = "insertFirstVendorDetails", author = "Anoj Kunes")
    fun insertFirstVendorDetailsToDb(mongoTemplate: MongoTemplate) {
        val vendor = Vendor("5e246ab08362930f9a15ec2c", "edi-vendor", "Edinburgh")
        mongoTemplate.save(vendor)
    }

    @ChangeSet(order = "02", id = "insertSecondVendorDetails", author = "Anoj Kunes")
    fun insertSecondVendorDetailsToDb(mongoTemplate: MongoTemplate) {
        val vendor = Vendor("5e246ab08362930f9a15ec2a", "lon-vendor", "London")
        mongoTemplate.save(vendor)
    }
}