package com.ls.lms.mongo.changelog

import com.github.mongobee.changeset.ChangeLog
import com.github.mongobee.changeset.ChangeSet
import com.ls.lms.mongo.entity.Customer
import org.springframework.data.mongodb.core.MongoTemplate

@ChangeLog
class CustomerMigrationChangeLog {
    @ChangeSet(order = "01", id = "insertFirstCustomerDetails", author = "Anoj Kunes")
    fun insertFirstCustomerDetailsToDb(mongoTemplate: MongoTemplate) {
        val customer = Customer("5e24676fae7d22144c65bab9", "anojkunes@googlemail.com", "Anoj", "Kunes", "London")
        mongoTemplate.save(customer)
    }

    @ChangeSet(order = "02", id = "insertSecondCustomerDetails", author = "Anoj Kunes")
    fun insertSecondCustomerDetailsToDb(mongoTemplate: MongoTemplate) {
        val customer = Customer("5e24676fae7d22144c65babb", "vanderlinde@googlemail.com", "Dutch", "Vanderlinde", "London")
        mongoTemplate.save(customer)
    }

    @ChangeSet(order = "03", id = "insertThirdCustomerDetails", author = "Anoj Kunes")
    fun insertThirdCustomerDetailsToDb(mongoTemplate: MongoTemplate) {
        val customer = Customer("5e24676fae7d22144c65babd", "arturmorgan@googlemail.com", "Artur", "Morgan", "Edinburgh")
        mongoTemplate.save(customer)
    }

    @ChangeSet(order = "04", id = "insertFourthCustomerDetails", author = "Anoj Kunes")
    fun insertFourthCustomerDetailsToDb(mongoTemplate: MongoTemplate) {
        val customer = Customer("5e24676fae7d22144c65babf", "sadieadler@googlemail.com", "Sadie", "Adler", "Edinburgh")
        mongoTemplate.save(customer)
    }
}