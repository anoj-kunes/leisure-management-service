package com.ls.lms.mongo.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Customer(
        @Id
        val id: String?,
        @Indexed(unique = true, sparse = true)
        val email: String,
        val firstName: String,
        val lastName: String,
        val city: String
)