package com.ls.lms.mongo.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Vendor(
        @Id
        val id: String?,
        val vendor: String,
        val city: String
)