package com.ls.lms.mongo.repository

import com.ls.lms.mongo.entity.Vendor
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface VendorRepository: MongoRepository<Vendor, String> {
}