package com.ls.lms.service

interface LeisureManagementService<T> {
    fun findById(id: String): T
}