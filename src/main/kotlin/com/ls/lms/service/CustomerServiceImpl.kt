package com.ls.lms.service

import com.ls.lms.dto.CustomerDto
import com.ls.lms.exception.DataNotFoundException
import com.ls.lms.mongo.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CustomerServiceImpl: LeisureManagementService<CustomerDto> {

    @Autowired
    private lateinit var customerRepository: CustomerRepository

    override fun findById(id: String): CustomerDto {
        val customer = customerRepository.findById(id)
                .orElseThrow { throw DataNotFoundException("Customer not found for #${id}") }

        return CustomerDto(id, customer.email, customer.firstName, customer.lastName, customer.city)
    }
}