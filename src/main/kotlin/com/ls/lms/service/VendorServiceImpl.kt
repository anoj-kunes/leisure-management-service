package com.ls.lms.service

import com.ls.lms.dto.VendorDto
import com.ls.lms.exception.DataNotFoundException
import com.ls.lms.mongo.repository.VendorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class VendorServiceImpl: LeisureManagementService<VendorDto> {
    @Autowired
    private lateinit var vendorRepository: VendorRepository

    override fun findById(id: String): VendorDto {
        val vendor = vendorRepository.findById(id)
                .orElseThrow { throw DataNotFoundException("Vendor not found for #${id}") }

        return VendorDto(vendor.id, vendor.vendor, vendor.city)
    }
}