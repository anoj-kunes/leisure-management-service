package com.ls.lms

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication

@SpringBootApplication
@EntityScan(value = arrayOf("com.ls.lms.mongo.entity"))
class Application

fun main(args: Array<String>) {
	runApplication<Application>(*args)
}
