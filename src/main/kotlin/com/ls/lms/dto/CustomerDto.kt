package com.ls.lms.dto

data class CustomerDto(
        val id: String?,
        val email: String,
        val firstName: String,
        val lastName: String,
        val city: String
)