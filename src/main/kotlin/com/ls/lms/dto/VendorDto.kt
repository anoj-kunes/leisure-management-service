package com.ls.lms.dto

data class VendorDto(
        val id: String?,
        val vendor: String,
        val city: String
)