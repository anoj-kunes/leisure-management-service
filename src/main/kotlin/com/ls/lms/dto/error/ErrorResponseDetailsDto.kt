package com.ls.lms.dto.error

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ErrorResponseDetailsDto (
        var error: String?
)