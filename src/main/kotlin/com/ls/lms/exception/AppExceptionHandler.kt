package com.ls.lms.exception

import com.ls.lms.dto.error.ErrorResponseDetailsDto
import com.ls.lms.dto.error.ErrorResponseDto
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class AppExceptionHandler: ResponseEntityExceptionHandler()  {

    @ExceptionHandler(value = arrayOf(DataNotFoundException::class))
    fun unhandledDataNotFoundException(ex: DataNotFoundException, request: WebRequest): ResponseEntity<Any> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val response = ErrorResponseDto("DATA_NOT_AVAILABLE", listOf(ErrorResponseDetailsDto(ex.message)))
        return handleExceptionInternal(ex, response, HttpHeaders(), HttpStatus.NOT_FOUND, request)
    }

    @ExceptionHandler(value = arrayOf((Throwable::class)))
    fun unhandledException(ex: Exception, request: WebRequest): ResponseEntity<Any> {
        val response = ErrorResponseDto(
                "INTERNAL_SERVER_ERROR",
                listOf(ErrorResponseDetailsDto(ex.message))
        )
        return handleExceptionInternal(ex, response, HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request)
    }
}