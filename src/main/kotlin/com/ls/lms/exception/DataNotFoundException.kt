package com.ls.lms.exception

import java.lang.RuntimeException

data class DataNotFoundException(override val message: String): RuntimeException(message)