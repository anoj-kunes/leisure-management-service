package com.ls.lms._config

import com.github.mongobee.Mongobee
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.mongodb.core.MongoTemplate

// mongo migration
@Configuration
class MongoBeeMigrationConfig {
    companion object {
        private const val MONGODB_CHANGELOG_PACKAGE_LCS = "com.ls.lms.mongo.changelog"
    }

    @Value("\${spring.data.mongodb.uri}")
    private val uaaUri: String? = null

    @Autowired
    private lateinit var mongoTemplateUaa: MongoTemplate

    @Primary
    @Bean
    fun mongoBeeUaa(): Mongobee {
        val runner = Mongobee(uaaUri)
        runner.setMongoTemplate(mongoTemplateUaa)
        runner.setChangeLogsScanPackage(MONGODB_CHANGELOG_PACKAGE_LCS)
        return runner
    }
}