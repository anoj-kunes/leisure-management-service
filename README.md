#Leisure Management Service and Leisure Pass Service API

## NOTES For Leisure Management Service API
This project is written in kotlin Spring Boot and uses MongoDB with the port (27017). The mongodb property can be changed in the `application-local.properties` and `application-unit-test.properties`. 
Leisure Management
~~~
spring.data.mongodb.uri=mongodb://localhost:27017/leisure_management_db
~~~
The API endpoints are protected using the Spring boot basic auth security (the username and password is specified in the `application-local.properties` and `application-unit-test.properties`.)
The endpoints in this project are mostly getting the information about a single resource of a Customer and Vendor. These endpoints are being consumed in Leisure-Pass-Service which issues tourist passes.
The idea of two different services is for scalability when there are new serviced which may depend on Customers, Vendor and Tourist passes so that it becomes more of a micro-service architecture.

## NOTES For Leisure Pass Service API
You need to run `leisure-management-service` in order to run `leisure-pass-service` (Leisure-Pass-Service requires to get customer and vendor information from Leisure-Management-Service)
Leisure Pass Service is written in Java 8 Spring boot and uses MongoDB. This service deals with creating new pass ticket, checking the ticket validity, renewing and cancelling the tickets. 
~~~
spring.data.mongodb.uri=mongodb://localhost:27017/leisure_pass_db
~~~

## Running Leisure-Pass-Service and Leisure-Management-Service
Go to Leisure-Pass-Service or Leisure-Management-Service project and use these commands below:
- use `mvn install`
- run `com.ls.lms.Application`(Leisure-Management-Service)  and `com.ls.lps.Application`(Leisure-Pass-Service) or use `mvn spring-boot:run` command

## Test
Both services uses Mockito and JUnit 5 to fully test the code coverage

## Postman
- Also Look at examples for each requests
### Leisure-Management-Service
~~~
https://www.getpostman.com/collections/c90e59ebc25ba1f82601
~~~
### Leisure-Pass-Service
~~~
https://www.getpostman.com/collections/7dfbe7dd415af292adc3
~~~

## Mongo Migration
Project already uses mongo bee migration dependency. It will migrate the data when running application

## Assumptions made
1. The pass length is in hours duration
2. Customer can have multiple tourist passes even if they are have one which is already active
3. Having customer and vendor as a separate service and it may be used for different projects
4. Pass duration can be between 1hr and 720hrs (1 Month)
5. Ticket Passes are deleted instead of archived for auditing
6. When issuing a new pass Ticket, its assumed that customer ID, Vendor ID and pass length is required.
7. There are 2 different databases. One database which stores only information about Tourist Passes and the other database
which deals with Customers and Vendors.
8. Leisure-Pass-Service is heavily validated on the customer ID and Vendor ID via micro-services before being inserted for a new Tourist Pass.
9. Home City field is assumed to be in the Customer's entity
10. IssuedAt and ExpiredAt date are viewed as ISO date time format

## Future Improvements
1. Security can be improved using OAUTH2 to protect resources and issuing access token
2. more Integration testing
3. Custom error response for unauthorized and unforbidden errors
4. Services could be improved using GraphQL instead of API so that there isn't much under or over fetching of a resource if project becomes bigger.
But this may be out of scope for code challenge.
5. Vendor Name can be unique code and be used instead of Vendor Id
6. Using Swagger Instead of Postman for API documentation
 